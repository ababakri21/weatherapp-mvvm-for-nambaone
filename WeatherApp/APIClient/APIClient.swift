import Foundation
import Alamofire
import INTULocationManager

class APIClient {
    
    static var shared: APIClient = APIClient()
    
    private var serverUrl: String = "https://api.darksky.net/forecast"
    private var apiKey: String = "2fddd4a62f33b40d48557b05c6a88adf"
    
    func retrieveForecast(with location: CLLocation, completionHandler: @escaping (_ success: Bool, _ forecast: Weather?, _ error: Error?) -> Void) {
        Alamofire.request("\(serverUrl)/\(apiKey)/\(location.coordinate.latitude),\(location.coordinate.longitude)").responseWeather { response in
            if let error = response.error {
                completionHandler(false, nil, error)
            } else if let forecast = response.result.value {
               completionHandler(true, forecast, nil)
            } else {
                completionHandler(false, nil, APIError.unkownError)
            }
        }
    }
    
    func retrieveForecastForCurrentLocation(completionHandler: @escaping (_ success: Bool, _ forecast: Weather?, _ error: Error?) -> Void) {
        let locationManager: INTULocationManager = INTULocationManager.sharedInstance()
        locationManager.requestLocation(withDesiredAccuracy: .city, timeout: 10.0, delayUntilAuthorized: true) { (currentLocation, achievedAccuracy, status) in
            guard let currentLocation = currentLocation else {
                completionHandler(false, nil, APIError.unableToLocateUser)
                return
            }
            self.retrieveForecast(with: currentLocation, completionHandler: completionHandler)
        }
    }
}

enum APIError: Error {
    case unkownError
    case unableToLocateUser
}
