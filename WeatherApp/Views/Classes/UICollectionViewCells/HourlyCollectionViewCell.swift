import UIKit

class HourlyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!
    var currentlyViewModel: CurrentlyViewModel? {
        didSet {
            updateView()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateView() {
        guard let currentlyViewModel = currentlyViewModel else { return }
        hourLabel.text = currentlyViewModel.timeAsHourText
        temperatureLabel.text = currentlyViewModel.temperatureText
        iconImageView.image = UIImage(named: currentlyViewModel.iconText)
    }

}
