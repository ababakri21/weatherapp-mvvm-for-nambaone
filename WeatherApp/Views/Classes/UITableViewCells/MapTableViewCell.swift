import UIKit
import MapKit

class MapTableViewCell: UITableViewCell, MKMapViewDelegate {
    
    @IBOutlet var map: MKMapView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        map.setUserTrackingMode(.follow, animated: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
