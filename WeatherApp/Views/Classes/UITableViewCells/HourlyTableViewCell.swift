import UIKit

class HourlyTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var hourlyCollectionView: UICollectionView!
    var hourlyViewModel: HourlyViewModel? {
        didSet {
            updateView()
        }
    }
    var hourlyDataArray: [Currently] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        hourlyCollectionView.register(UINib(nibName: "HourlyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HourlyCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateView() {
        guard let hourlyViewModel = hourlyViewModel else { return }
        hourlyDataArray = hourlyViewModel.hourlyDataArray
        hourlyCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hourlyDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HourlyCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourlyCollectionViewCell", for: indexPath) as! HourlyCollectionViewCell
        cell.currentlyViewModel = CurrentlyViewModel(currently: hourlyDataArray[indexPath.item])
        return cell
    }
    
}
