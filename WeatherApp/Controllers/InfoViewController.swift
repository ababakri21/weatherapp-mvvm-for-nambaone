import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet var precipitationLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var windspeedLabel: UILabel!
    @IBOutlet var cloudcoverLabel: UILabel!
    @IBOutlet var humidityLabel: UILabel!
    var currentlyViewModel: CurrentlyViewModel?
    
    static func instantiate() -> InfoViewController {
        return UIStoryboard(name: "InfoViewController", bundle: nil).instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        styleController()
        updateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func styleController() {
        navigationController?.navigationBar.tintColor = .white
    }
    
    func updateView() {
        guard let currentlyViewModel = currentlyViewModel else { return }
        summaryLabel.text = currentlyViewModel.summaryText
        precipitationLabel.text = currentlyViewModel.precipProbabilityText
        temperatureLabel.text = currentlyViewModel.temperatureText
        windspeedLabel.text = currentlyViewModel.windspeedText
        cloudcoverLabel.text = currentlyViewModel.cloudCoverText
        humidityLabel.text = currentlyViewModel.humidityText
    }
}
