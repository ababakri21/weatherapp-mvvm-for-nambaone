import UIKit
import MBProgressHUD

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var layoutTableView: UITableView!
    var retrievedForecast: Weather? {
        didSet {
            layoutTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        styleController()
        
        //Register UITableView XIBs
        layoutTableView.register(UINib(nibName: "MapTableViewCell", bundle: nil), forCellReuseIdentifier: "MapTableViewCell")
        layoutTableView.register(UINib(nibName: "CurrentlyTableViewCell", bundle: nil), forCellReuseIdentifier: "CurrentlyTableViewCell")
        layoutTableView.register(UINib(nibName: "HourlyTableViewCell", bundle: nil), forCellReuseIdentifier: "HourlyTableViewCell")
        layoutTableView.register(UINib(nibName: "DailyTableViewCell", bundle: nil), forCellReuseIdentifier: "DailyTableViewCell")

        retrieveForecastData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func styleController() {
        // Styles the view controller
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        
        let refreshBarButonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(retrieveForecastData))
        navigationItem.rightBarButtonItem = refreshBarButonItem
        navigationController?.navigationBar.tintColor = .white
    }
    
    @objc func retrieveForecastData() {
        // Retrieves data from Dark Sky and readies for presentation
        MBProgressHUD.showAdded(to: self.view, animated: true)
        APIClient.shared.retrieveForecastForCurrentLocation { (success, forecast, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("[API Error]", error.localizedDescription)
            } else if let forecast = forecast {
                self.retrievedForecast = forecast
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return retrievedForecast == nil ? 0 : 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return retrievedForecast?.daily?.data?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: MapTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MapTableViewCell") as! MapTableViewCell
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell: CurrentlyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CurrentlyTableViewCell") as! CurrentlyTableViewCell
            if let currently = retrievedForecast?.currently {
                cell.currentlyViewModel = CurrentlyViewModel(currently: currently)
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell: HourlyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HourlyTableViewCell") as! HourlyTableViewCell
            if let hourly = retrievedForecast?.hourly {
                cell.hourlyViewModel = HourlyViewModel(hourly: hourly)
            }
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell: DailyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DailyTableViewCell") as! DailyTableViewCell
            if let dailyDatum = retrievedForecast?.daily?.data?[safe: indexPath.row] {
                cell.dailyDatumViewModel = DailyDatumViewModel(dailyDatum: dailyDatum)
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 150.0
        case 1:
            return 60.0
        case 2:
            return 92.0
        case 3:
            return 52.0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch indexPath.section {
        case 1:
            let infoViewController: InfoViewController = InfoViewController.instantiate()
            infoViewController.title = NSLocalizedString("Info", comment: "")
            if let currently = retrievedForecast?.currently {
                infoViewController.currentlyViewModel = CurrentlyViewModel(currently: currently)
            }
            navigationController?.pushViewController(infoViewController, animated: true)
        default:
            return
        }
    }
}
