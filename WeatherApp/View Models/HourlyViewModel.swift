import Foundation

class HourlyViewModel {
    
    var hourly: Hourly
    
    init(hourly: Hourly) {
        self.hourly = hourly
    }
    
    var hourlyDataArray: [Currently] {
        guard let hourlyDataArray = hourly.data else {
            return []
        }
        var filteredHourlyDataArray: [Currently] = []
        
        for currently in hourlyDataArray {
            filteredHourlyDataArray.append(currently)
            
            if (filteredHourlyDataArray.count > 23) {
                break
            }
        }
        return filteredHourlyDataArray
    }
    
}
